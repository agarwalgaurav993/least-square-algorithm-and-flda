'''
Created on 22 Feb, 2017

@author: gaurav
'''
import numpy as np
import pylab as pl
# training data
def dataSet1():
    cls1 = np.array([(3, 3), (3, 0), (2, 1), (0, 2)], float)
    cls2 = np.array([(-1, 1), (0, 0), (-1, -1), (1, 0)], float)
    return cls1, cls2

def dataSet2():
    cls1 = np.array([(3, 3), (3, 0), (2, 1), (0, 1.5)], float)
    cls2 = np.array([(-1, 1), (0, 0), (-1, -1), (1, 0)], float)
    return cls1, cls2
def testData():
    cls1 = np.array([(4, 2), (2, 4), (2, 3), (3, 6), (4, 4)], float)
    cls2 = np.array([(9, 10), (6, 8), (9, 5), (8,7), (10, 8)], float)
    return cls1, cls2
def augment_dataSet(X1, X2):
    trainSet = np.vstack((X1,X2))
    Y = np.vstack((np.ones((len(X1), 1)), -np.ones((len(X2), 1))))
    bias = np.ones((len(trainSet), 1))
    trainSet = np.hstack((trainSet, bias))
    return trainSet, Y

def pointsPlotting(dataSet, col, figHandle, xlabel, ylabel):
    #Plotting Dataset
    pl.figure(figHandle)
    pl.xlabel(xlabel)
    pl.ylabel(ylabel)
    ax = pl.subplot(111)
    ax.scatter(dataSet[:, 0], dataSet[:,1], c=col)

def pointsonLine(dataSet, col, figHandle, xlabel, ylabel):
    pl.figure(figHandle)
    pl.xlabel(xlabel)
    pl.ylabel(ylabel)
    ax = pl.subplot(111)
    ax.scatter(dataSet,[0 for x in dataSet] , c=col)
def getpointsOnVector(V):
    a, b = V
    x1, x2 = -2*a, 5*a
    y1, y2 = -2*b, 5*b
    return [x1, x2], [y1,y2]

def getpointsOnLine(abc):
    a, b, c = abc
    slope, intercept = -(a/b), -(c/b)
    x1, y1 = 5.0, slope*5.0 + intercept
    x2, y2 = -2.0, slope*-2.0 + intercept
    return [x1, x2], [y1, y2]

def linePlotting(x, y, figHandle, xlabel, ylabel, l):
    pl.figure(figHandle)
    pl.xlabel(xlabel)
    pl.ylabel(ylabel)
    ax = pl.subplot(111)
    ax.plot(x, y, label=l)

Contains:
1. Least Mean Square Error Classifier
2. Fishers Linear Discriminant Analysis Classifier
3. Controller.py
It executes both FLDA and LMS on a particular data-set and plots the classifier obtained by each
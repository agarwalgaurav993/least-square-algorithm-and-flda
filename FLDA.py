'''
Created on 22 Feb, 2017

@author: gaurav
'''
import pylab as pl
import numpy as np
from numpy.linalg import inv, eig, norm
import dataSource as ds
from dataSource import getpointsOnVector, getpointsOnLine
figHandle = "FLDA"
xlabel = "X1"
ylabel = "X2"
def classCovariance(X, mean):
    S = np.zeros((X.shape[1], X.shape[1]))
    for x in X:
        temp = np.outer((x - mean),(x - mean))
        S += temp
    return S
    
def classProjection(W, X):
    cls = []
    for x in X:
        cls.append(np.inner(x, W)/norm(W))
    return cls
def FLDA_Algorithm(cData1, cData2):
    m1 = np.mean(cData1, axis = 0, dtype = np.float)
    m2 = np.mean(cData2, axis = 0, dtype = np.float)
    Sw = classCovariance(cData1, m1) + classCovariance(cData2, m2)
    Sb = np.outer(m1 - m2, m1 - m2)
    mat = inv(Sw).dot(Sb)
    w, v = eig(mat)
    pV = v[:,0]#Projection Vector
    print "Projection Vector : ", pV
    return pV

def getC(cls1, cls2, W):
    i = (min(cls1) + max(cls2))/2
    pt = W*(i/norm(W))
    return -(np.dot(pt,W))
    #return tuple(W*(i/norm(W)))

def getprojectedPoints(W,class1, class2):
    points = []
    for i in class1 + class2:
        points.append(tuple(W*(i/norm(W))))
    return np.array(points)

def main():
    cls1, cls2 = ds.dataSet2()
    ds.pointsPlotting(cls1, "blue", figHandle , xlabel, ylabel)
    ds.pointsPlotting(cls2, "red", figHandle, xlabel, ylabel)
    projectionVector = FLDA_Algorithm(cls1, cls2)
    #Class Projection
    cls1 = classProjection(projectionVector, cls1)
    cls2 = classProjection(projectionVector, cls2)
    x, y = getpointsOnVector(projectionVector)
    ds.linePlotting(x, y, figHandle, xlabel, ylabel, l = "Class Projection Line")
    #FLDA Class Separation Line
    a, b = projectionVector
    c = getC(cls1, cls2, projectionVector)
    x, y = getpointsOnLine([a,b,c])
    ds.linePlotting(x, y, figHandle, xlabel, ylabel, l = "Class Separation Line")
    pl.legend()
    #1-D View of Projection Line
    ds.pointsonLine(cls1, "blue", "1-D View of Projection Line", "w vector", "")
    ds.pointsonLine(cls2, "red", "1-D View of Projection Line", "w vector", "")
    ds.pointsonLine([(min(cls1) + max(cls2))/2], "yellow", "1-D View of Projection Line", "w vector", "")
    print "Projected Class 1 :", cls1
    print "Projected Class 2 :", cls2
    pl.legend()
    pl.show()

    
if __name__ == '__main__':
    main()
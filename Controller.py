'''
Created on 23 Feb, 2017

@author: gaurav
'''
import dataSource as ds
import LMS
import pylab as pl
import FLDA
figHandle="LMS-FLDA"
xlabel = "Feature X1"
ylabel = "Feature X2"
def main():
    cls1, cls2 = ds.dataSet2()
    ds.pointsPlotting(cls1, 'r', figHandle, xlabel , ylabel)
    ds.pointsPlotting(cls2, 'b', figHandle, xlabel , ylabel)
    #LMS
    X, Y = ds.augment_dataSet(cls1, cls2)
    abc = LMS.LeastSquareD(X, Y)
    #FLDA
    pV = FLDA.FLDA_Algorithm(cls1, cls2)
    clp1 = FLDA.classProjection(pV, cls1)
    clp2 = FLDA.classProjection(pV, cls2)
    a, b = pV
    c = FLDA.getC(clp1, clp2, pV)
    x1, y1 = ds.getpointsOnLine(abc)
    x2, y2 = ds.getpointsOnLine([a,b,c])
    x3, y3 = ds.getpointsOnVector(pV)
    ds.linePlotting(x1, y1, figHandle, xlabel , ylabel, l = "LMS-Class Separation Line")
    ds.linePlotting(x2, y2, figHandle, xlabel, ylabel, l = "FLDA-Class Seperation Line")
    ds.linePlotting(x3, y3, figHandle, xlabel, ylabel, l = "FLDA-Projection Line")
    pl.legend()
    pl.show()
    
if __name__ == '__main__':
    main()
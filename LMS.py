'''
Created on 22 Feb, 2017

@author: gaurav
'''
import pylab as pl
import numpy as np
from numpy.linalg import inv
import dataSource as ds


def sig(num):
    if num < 0.:
        return -1
    elif num == 0.0:
        return 0
    else:
        return 1

def LeastSquareD(X, Y):
    W = ((inv(((X.T).dot(X))).dot(X.T)).dot(Y)).T
    print W[0, :]
    return W[0, :]
def LeastSquareI(X, Y):
    W = np.zeros((1,X.shape[1]))
    i = 0
    while i< len(X):
        x = X[i]
        fx = W.dot(x)
        if Y[i]*sig(fx) <= 0:
            oW = W
            W = W + (Y[i] - sig(fx))*x
            if (np.absolute(np.subtract(oW ,W)) <= 0.001).sum() == W.size:
                print W
                return W
            i = 0
        else:
            i += 1
    print W[0,:]
    return W[0, :]
                
def main():
    cls1, cls2 = ds.dataSet2()
    X, Y = ds.augment_dataSet(cls1, cls2)
    abc1= LeastSquareD(X, Y)
    abc2 = LeastSquareI(X, Y)
    x1, y1 = ds.getpointsOnLine(abc1)
    x2, y2 = ds.getpointsOnLine(abc2)
    ds.pointsPlotting(cls1, 'r', figHandle="LMS", xlabel = "Feature X1", ylabel = "Feature X2")
    ds.pointsPlotting(cls2, 'b', figHandle="LMS", xlabel = "Feature X1", ylabel = "Feature X2")
    ds.linePlotting(x1, y1, figHandle="LMS", xlabel = "Feature X1", ylabel = "Feature X2", l = "Formula Based")
    ds.linePlotting(x2, y2, figHandle="LMS", xlabel = "Feature X1", ylabel = "Feature X2", l = "Iterative Widro- Hoff")
    pl.legend()
    pl.show()
    
    
if __name__ == '__main__':
    main()

